package test_helpers

import (
	// "fmt"
	"reflect"
	"strings"
	"testing"
)

func TestCompareFiles(t *testing.T) {
	testCompareFiles := map[string]struct {
		input  map[string]interface{}
		result bool
	}{
		"fail": {
			input: map[string]interface{}{
				"input1": "test/input/file1.test",
				"input2": "test/input/file2.test",
			},
			result: false,
		},
		"nofile_success": {
			input: map[string]interface{}{
				"input1": "test/input/none",
				"input2": "test/input/file1.test",
			},
			result: true,
		},
		"success": {
			input: map[string]interface{}{
				"input1": "test/input/file1.test",
				"input2": "test/input/file3.test",
			},
			result: true,
		},
		"nofile2_success": {
			input: map[string]interface{}{
				"input1": "test/input/file1.test",
				"input2": "test/input/none",
			},
			result: true,
		},
	}
	for name, test := range testCompareFiles {
		test := test
		t.Run(name, func(t *testing.T) {
			if strings.Contains(name, "nofile") {
				TestOsExit(t, "TestCompareFiles", func(t *testing.T) {
					_ = CompareFiles(test.input["input1"].(string), test.input["input2"].(string))
				})
			} else {
				got := CompareFiles(test.input["input1"].(string), test.input["input2"].(string))
				if !reflect.DeepEqual(got, test.result) {
					t.Errorf("FAILED : testCompareFiles(%q)  \nreturned %t; \nexpected %t", test.input, got, test.result)
				} else {
					t.Logf("SUCCEDED : \"testCompareFiles('%q')\", \ngot -> %t, \nexpected -> %t", test.input, got, test.result)
				}
			}

		})
	}

}

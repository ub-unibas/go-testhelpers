# TestHelpers

| Branch        | Pipeline          | Code coverage  |  Latest tag  |
| ------------- |:-----------------:|:--------------:| ------------:|
| main       | [![pipeline status](https://gitlab.switch.ch/ub-unibas/go-testhelpers/badges/main/pipeline.svg)](https://gitlab.switch.ch/ub-unibas/go-testhelpers/-/commits/main)  | [![coverage report](https://gitlab.switch.ch/ub-unibas/go-testhelpers/badges/main/coverage.svg)](https://gitlab.switch.ch/ub-unibas/go-testhelpers/-/commits/main) | [latest tag](https://gitlab.switch.ch/ub-unibas/go-testhelpers/-/tags)

## Description
This is a library to help testing functions

## Usage
- Compares two files and returns true if both files are identical else false
```
func CompareFiles(file1, file2 string) bool 
```

- To test os.exist() or when we call panic / fatal

```
func TestOsExit(t *testing.T, funcName string, testFunc func(*testing.T)) 
```


## Support
Please contact Paul Nguyen

## Authors and acknowledgment
Paul Nguyen

## Project status
Development

## Dependencies

### Internal
none

### External
none

## Test 
to run test :  
```
go test -v ./... -coverprofile=coverage.out && go tool cover -html=coverage.out
```

## License